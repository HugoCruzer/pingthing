#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from sys import argv
import sys, os, datetime
from django.conf import settings
import sqlite3 as lite
from django.core.management import call_command

class Command(BaseCommand):
    args = 'None Required'
    help = 'Ping all pings defined in database'

    def handle(self, *args, **options):
        
        con = None
        
        try:
            #Load ping definitions from the database
            con = lite.connect(settings.DATABASES['default']['NAME'])
            with con:
                cur = con.cursor()
                cur.execute("SELECT protocol, target_ip, target_port, id FROM pingthing_pingdefinition")
                rows = cur.fetchall()
                for row in rows:
                    call_command('pingthis', row[0], row[1], row[2], row[3])
                
        # Error Handling            
        except lite.Error, e:
            
            print "Error %s:" % e.args[0]
            sys.exit(1)

        # Always close DB connection    
        finally:
            
            if con:
                con.close()