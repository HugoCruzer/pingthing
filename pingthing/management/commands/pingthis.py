#!/usr/bin/python
# -*- coding: utf-8 -*-
# Base script from Motoma - http://motoma.io/basic-server-monitoring-with-python/

from django.core.management.base import BaseCommand, CommandError
from urllib2 import urlopen
from socket import socket
from sys import argv
import sys, os, datetime
from django.conf import settings
import sqlite3 as lite

class Command(BaseCommand):
    args = '<protocol - tcp or http>, <target ip>, <target port>, <pid>'
    help = 'Ping this!'

    def handle(self, *args, **options):
        protocol = args[0]
        target_ip = args[1]
        target_port = args[2]
        target = str(target_ip)+':'+str(target_port)
        ping_id = args[3]
        
        con = None
         
        def tcp_test(server_info):
            cpos = server_info.find(':')
            try:
                sock = socket()
                sock.connect((server_info[:cpos], int(server_info[cpos+1:])))
                sock.close
                return True
            except:
                return False
         
        def http_test(server_info):
            try:
                data = urlopen(server_info).read()
                return True
            except:
                return False
         
        def server_test(test_type, target):
            if test_type.lower() == 'tcp':
            	server_info = target
                return tcp_test(server_info)
            elif test_type.lower() == 'http':
            	server_info = test_type.lower() + '://' + target
                return http_test(server_info)        
        
        try:
        
            #Main
            if len(args) != 4:
                ping_result = 'Failed'
            else:
                if not server_test(protocol, target):
                    ping_result = 'Failed'
                else:
                    #return 'Success'
                    ping_result = 'Success'
            
            #Update the database
            con = lite.connect(settings.DATABASES['default']['NAME'])
            with con:
                cur = con.cursor()
                cur.execute("INSERT INTO pingthing_pingresult (pingdef_id, results, date) VALUES(?,?,?)", (ping_id, ping_result, datetime.datetime.now()))
                
            return ping_result
        
        # Error Handling            
        except lite.Error, e:
            
            print "Error %s:" % e.args[0]
            sys.exit(1)

        # Always close DB connection    
        finally:
            
            if con:
                con.close()