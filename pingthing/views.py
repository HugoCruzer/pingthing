from django.core.management import call_command
from StringIO import StringIO
from django.http import HttpResponse,HttpResponseRedirect
from django.db import connection
from pingthing.models import PingDefinition, PingResult, PingClassification
from django.shortcuts import render
from django.core import management

def pingListView(request, class_id):  
    #Breadcrumb for navigation
    pping = PingClassification.objects.filter(id=class_id)
    pingthing_parentpingID = pping[0].parent.id
    #Current classification (to be used if no folder)
    pingthing_currentping = pping[0]
    #Actual ping data
    pingthing_classlist = PingClassification.objects.filter(parent=class_id).exclude(title='root')
    pingthing_pinglist = PingDefinition.objects.extra(
    select={
        'results': 'SELECT results FROM pingthing_pingresult WHERE pingthing_pingdefinition.id = pingthing_pingresult.pingdef_id ORDER BY date DESC',
        'date': 'SELECT date FROM pingthing_pingresult WHERE pingthing_pingdefinition.id = pingthing_pingresult.pingdef_id ORDER BY date DESC'
    }).filter(category=class_id)
    #Display
    return render(request, 'pingthing/ping_display.html', {'pingthing_pinglist': pingthing_pinglist, 'pingthing_classlist': pingthing_classlist, 'pingthing_parentpingID': pingthing_parentpingID, 'pingthing_currentping': pingthing_currentping})
    
def pingRefreshView(request, ping_id, class_id):
    ping_select = PingDefinition.objects.get(pk=ping_id)
    
    management.call_command('pingthis', ping_select.protocol, ping_select.target_ip, ping_select.target_port, ping_id)
    
    return HttpResponseRedirect('/'+class_id+'/depth')
    