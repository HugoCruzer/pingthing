from django.contrib import admin
from pingthing.models import PingDefinition, PingResult, PingClassification

admin.site.register(PingClassification)
admin.site.register(PingDefinition)
admin.site.register(PingResult)

