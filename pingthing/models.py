from django.db import models
import datetime
from django.conf import settings

class PingClassification(models.Model):
    title = models.CharField(max_length=30, help_text='Maximum 30 characters.')
    parent = models.ForeignKey('self')
    
    def __unicode__(self):
        #return self.title
        allRecords = PingClassification.objects.get(id=self.parent.id)
        breadcrumb = self.title
        while allRecords.title <> 'root':
            breadcrumb = allRecords.title+'-'+breadcrumb 
            allRecords = PingClassification.objects.get(id=allRecords.parent.id)
        return breadcrumb
    
    def get_absolute_url(self):
        return settings.SITE_URL+str(self.id)+'/depth/'

PROTOCOL_CHOICES = (('HTTP','HTTP'), ('TCP','TCP'))
        
class PingDefinition(models.Model):
    title = models.CharField(max_length=250, help_text='Maximum 250 characters.')
    summary = models.TextField(blank=True, help_text='An optional summary.')
    target_ip = models.CharField(max_length=30, help_text='Maximum 30 characters.')
    target_port = models.IntegerField()
    category = models.ForeignKey(PingClassification)
    protocol = models.CharField(max_length=4, choices=PROTOCOL_CHOICES)

    def __unicode__(self):
        return self.title
    
    def get_absolute_url(self):
        return settings.SITE_URL+str(self.category.id)+'/depth/'

class PingResult(models.Model):
    pingdef = models.ForeignKey(PingDefinition)
    results = models.TextField(max_length=250, help_text='Maximum 250 characters.')
    date = models.DateTimeField(default=datetime.datetime.now)