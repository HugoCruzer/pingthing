from django.test import TestCase
from django.conf import settings

#Front-End
class DefaultViewTestCase(TestCase):
    def test_index(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 301)

#Management Commands
from django.core import management
from django.core.management import call_command

class HTTPPingTestCase(TestCase):
    def test_index(self):
        #Define some variables
        protocol='HTTP'
        target_ip='www.google.com'
        target_port='80'
        id=1
              
        #Call command file passing test path parameter
        call_command('pingthis', protocol, target_ip, target_port, id)   