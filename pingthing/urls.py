from django.conf.urls import patterns, include, url
from pingthing import views
from django.views.generic import RedirectView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pingthing.views.home', name='home'),
    # url(r'^pingthing/', include('pingthing.foo.urls')),
    
    url(r'^$', RedirectView.as_view(url='/1/depth/')),
    url(r'^(?P<class_id>\d+)/depth/', views.pingListView, name='ping_list'),
    url(r'^(?P<ping_id>\d+)/run/(?P<class_id>\d+)', views.pingRefreshView, name='ping_run'),
    
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
